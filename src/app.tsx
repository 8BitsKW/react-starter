import React, { useEffect, useState } from "react"

import ReactDOM from "react-dom"
import { BrowserRouter, Outlet, Route, Routes } from "react-router-dom"

import UserContext from "@contexts/UserContext"

import Login from "./screens/Login"
import Things from "./screens/Things"

import "core-js/stable"
import "regenerator-runtime/runtime"
import "./index.css"

/**
 * Global Layout for the application
 * @param onLogout when the user clicks logout button
 * @returns
 */
const Container = ({ onLogout }: { onLogout: () => void }): React.ReactElement => {
    return (
        <div className="w-full h-full flex flex-col">
            <div className="fixed top-0 left-0 right-0 h-16 bg-gray-400 dark:bg-gray-800 flex justify-between items-center p-2 z-10">
                <p>Logo</p>
                <button
                    className="block bg-blue-500 px-4 py-2 text-white hover:bg-blue-600 rounded-md self-end"
                    onClick={onLogout}>
                    Logout
                </button>
            </div>
            <div className="pt-16 flex flex-1">
                <Outlet />
            </div>
        </div>
    )
}

/**
 * Uses a React Router to switch patchs
 *
 * Wraps the authenticated (user exists) in a Container component
 * for the global style/layout of the app
 *
 * @see Container
 * @returns
 */
const App = (): React.ReactElement => {
    const [user, setUser] = useState<UserObject | undefined>(undefined)

    useEffect(() => {
        const stored = localStorage.getItem("user")
        if (stored && stored != "undefined") setUser(JSON.parse(stored))
    }, [])

    useEffect(() => {
        localStorage.setItem("user", JSON.stringify(user))
    }, [user])

    const handleLogout = (): void => {
        setUser(undefined)
    }

    if (!user) return <Login onLogin={setUser} />

    return (
        <UserContext.Provider value={user}>
            <BrowserRouter>
                <Routes>
                    <Route path="/" element={<Container onLogout={handleLogout} />}>
                        <Route path="/things" element={<Things />} />
                    </Route>
                </Routes>
            </BrowserRouter>
        </UserContext.Provider>
    )
}

ReactDOM.render(<App />, document.getElementById("root"))
