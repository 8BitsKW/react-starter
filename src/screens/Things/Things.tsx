import React, { useEffect, useState, useContext } from "react"

/* Each Screen should have its own API */
import { API } from "@api/Sample"
import UserContext from "@contexts/UserContext"

const Thing = ({ t }: { t: Something }): React.ReactElement => (
    <div className="flex flex-row">
        <h1>{t.id}</h1>
        <h1>{t.name}</h1>
    </div>
)

const Things = (): React.ReactElement => {
    const [things, setThings] = useState<Something[]>([])
    const [loading, setLoading] = useState(true)
    const user = useContext(UserContext)

    /* fetch the things on mount */
    useEffect(() => {
        API.GetListSomething(user!, (t: Something[]) => {
            setLoading(false)
            setThings(t)
        })
    }, [])

    if (loading)
        return <div className="w-full h-full flex justify-center items-center">Loading</div>

    return (
        <div className="flex flex-1 flex-col justify-center items-center">
            <h1 className="text-6xl font-bold mb-5">My Things</h1>
            {things.map((t) => (
                <Thing key={t.id} t={t} />
            ))}
        </div>
    )
}

export default Things
